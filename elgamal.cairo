# Declare this file as a StarkNet contract and set the required
# builtins.
%lang starknet
%builtins pedersen range_check

from starkware.cairo.common.cairo_builtins import HashBuiltin
from starkware.cairo.common.math_cmp import is_le, is_not_zero


const GENERATOR_X = 874739451078007766457464989774322083649278607533249481151382481072868806602
const GENERATOR_Y = 152666792071518830868575557812948353041420400780739481342941381225525861407
const GROUP_SIZE = 3618502788666131213697322783095070105623107215331596699973092056135872020481
const ALPHA = 1

struct ECPoint:
    member x : felt
    member y : felt
end

@storage_var
func pub_key() -> (res : ECPoint):
end

@storage_var
func priv_key() -> (res : felt):
end

@constructor
func constructor{
        syscall_ptr : felt*, pedersen_ptr : HashBuiltin*,
        range_check_ptr}(pub: ECPoint):

    pub_key.write(value=pub)
    return ()
end


func set_priv_key{
        syscall_ptr : felt*, pedersen_ptr : HashBuiltin*,
        range_check_ptr}(priv : felt):
    priv_key.write(value=priv)
    return ()
end


func ec_add{
        syscall_ptr : felt*, pedersen_ptr : HashBuiltin*,
        range_check_ptr}(point1: ECPoint, point2: ECPoint) -> (res: ECPoint):
    if point1.x == point2.x:
        assert 1 = 2
    let m = ((point1.y - point2.y) / (point1.x - point2.x))
    let x = (m * m) - point1.x - point2.x
    let y = (m * (point1.x - x)) - point1.y
    return ECPoint(x=x, y=y)


func ec_double{
        syscall_ptr : felt*, pedersen_ptr : HashBuiltin*,
        range_check_ptr}(point: ECPoint) -> (res: ECPoint):
    # Doubles a point on an elliptic curve with the equation y^2 = x^3 + alpha*x + beta mod p.
    # Assumes the point is given in affine form (x, y) and has y != 0.

    if point.y == 0:
        assert 1 = 2
    let m = (3 * point.x * point.x + ALPHA) / (2 * point.y)
    let x = (m * m) - (2 * point.x)
    let y = (m * (point.x - x)) - point.y
    return ECPoint(x=x, y=y)


func ec_mult{
        syscall_ptr : felt*, pedersen_ptr : HashBuiltin*,
        range_check_ptr}(scalar: felt, point: ECPoint) -> (res: ECPoint):
    # Multiplies by m a point on the elliptic curve with equation y^2 = x^3 + alpha*x + beta mod p.
    # Assumes the point is given in affine form (x, y) and that 0 < m < order(point).

    if scalar == 1:
        return (point)
    let even = is_le(scalar / 2, scalar)
    # if scalar % 2 == 0
    if even == True:
        return ec_mult(scalar / 2, ec_double(point)) # division / 2 works like Z here
    return ec_add(ec_mult(scalar - 1, point), point)
end


func decrypt_message{
        syscall_ptr : felt*, pedersen_ptr : HashBuiltin*,
        range_check_ptr} (cipher1 : ECPoint, cipher2: ECPoint) -> (res: ECPoint):
    let (priv) = priv_key.read()
    assert is_not_zero(priv) = True
    let s = ec_mult(priv, cipher1)
    let (m_x) = cipher2.x + s.x
    let (m_y) = cipher2.y - s.y
    return ECPoint(x=m_x, y=m_y)
end
