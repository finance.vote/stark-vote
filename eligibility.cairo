%lang starknet

from starkware.cairo.common.alloc import alloc
from starkware.cairo.common.cairo_builtins import HashBuiltin
from starkware.cairo.common.math import assert_nn
from starkware.starknet.common.messages import send_message_to_l1

// NFT contract address
const L1_CONTRACT_ADDRESS = (
    0x2Db8c2615db39a5eD8750B87aC8F217485BE11EC)
